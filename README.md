# voxl-docker-mavsdk-cpp

Example of how to use MAVSDK C++ in a Docker container running on target.

## Summary

By following this example project, you will:

- Create a docker image on VOXL
- Build the C++ MAVSDK and an a [slightly modified](https://gitlab.com/voxl-public/voxl-docker-images/voxl-docker-mavsdk-cpp/-/commit/1a2a6efe98380f97337ba56a19cacfa321b9ad80) example [takeoff and land](https://mavsdk.mavlink.io/develop/en/examples/takeoff_and_land.html) program 
- Execute the program by utlizing [voxl-vision-px4](https://gitlab.com/voxl-public/voxl-vision-px4)

## Prerequisites

- VOXL Flight or VOXL + Flight Core hardware
- For installation, the hardware needs to be connected to the internet, see [WiFi Station Mode configuration](https://docs.modalai.com/wifi-setup/#configure-station-mode) for more details
- [voxl-vision-px4](https://gitlab.com/voxl-public/voxl-vision-px4) needs to be configured using the [user guide](https://docs.modalai.com/voxl-vision-px4-installation/)
  - Note: we need to setup MAVLInk to route locally, edit `/etc/modalai/voxl-vision-px4.conf` file with the following:
    - v0.6.1 or newer:
    ```bash
    "en_localhost_mavlink_udp":	true,
    ```
    - older version:
    ```bash
    "en_secondary_qgc": true,
    "secondary_qgc_ip": "127.0.0.1",
    ```
- ADB is required for the build script to function properly, see [adb setup instructions](https://docs.modalai.com/setup-adb/) for details.

## Pull Image from Registry

The docker image for this example is hosted in our registry and can be pulled onto VOXL.  Ensure VOXL is connected to the internet by setting  up VOXL into [Station Mode](https://docs.modalai.com/wifi-setup/#configure-station-mode).  Once connceted to the internet, proceed with the following:

```bash
adb shell
```

Now on VOXL, enable docker support:

```bash
voxl-configure-docker-support.sh
```

Pull the image:

```bash
docker pull gcr.io/modalai-public/voxl-mavsdk-cpp:v1.0
```

*Note: If you'd like to build the image yourself, there are instructions at the end of this README.*

## Execute the Example Program

### Overview

After the docker image is build following the steps above, we can SSH onto the vehicle over WiFi and then execute the example program from the container.  The program in the container communicates using UDP to voxl-vision-px4 and then over UART to PX4 running on the flight controller (the STM32 on VOXL Flight or Flight Core).

### Usage

#### Warning

This example instructs the vehicle to takeoff to an altitude of roughly 2.5m, hold for several seconds, and then land.

#### Connect to Network

We have alraedy setup VOXL in [Station Mode](https://docs.modalai.com/wifi-setup/#configure-station-mode) when we downloaded the image.  

Run `ifconfig` to get the IP address of VOXL.  The following will assume the VOXL has an IP address of `192.168.1.100`.

- SSH onto VOXL using the following:

```bash
me@mylaptop:~$ ssh root@192.168.1.100
(password: oelinux123)
```

- Open a terminal in the container using `docker run`:

  *Note: if you built the image locally, you need to use the image name you build with, e.g. `voxl-mavsdk-cpp:v1.0`*

```bash
docker run -it --rm --privileged --net=host gcr.io/modalai-public/voxl-mavsdk-cpp:v1.0 /bin/bash

root@apq8096:/home/MAVSDK/examples/takeoff_land# 
```

#### Run Example

**NOTE:** in order for me to get the demo to run, I had to change `takeoff_and_land.cpp` at [this line](https://github.com/mavlink/MAVSDK/blob/develop/examples/takeoff_land/takeoff_and_land.cpp#L104):

Here's the [commit](https://gitlab.com/voxl-public/voxl-docker-images/voxl-docker-mavsdk-cpp/-/commit/1a2a6efe98380f97337ba56a19cacfa321b9ad80)

Otherwise, in my indoor flight setup, the value of `is_home_position_ok` would stay at `false` causing the `telemetry->health_all_ok()` to fail.

Now, run the example **if you have a safe flight area**:
- Note: for voxl-vision-px4 0.6.1 or newer, user port 14551.  For older versions, use port 14550.

```bash
 ./takeoff_and_land udp://:14551
```

Here's an example of the output:

```bash
root@apq8096:/home/MAVSDK/examples/takeoff_land# ./takeoff_and_land udp://:14551
[07:00:28|Info ] MAVSDK version: 0.28.0 (mavsdk_impl.cpp:26)
[07:00:28|Debug] New: System ID: 0 Comp ID: 0 (mavsdk_impl.cpp:377)
[07:00:28|Info ] New system on: 127.0.0.1:53745 (udp_connection.cpp:256)
Waiting to discover system...
[07:00:28|Debug] Component Autopilot (1) added. (system_impl.cpp:352)
[07:00:29|Debug] Discovered 1 component(s) (UUID: 3546673866589352499) (system_impl.cpp:525)
Discovered system with UUID: 3546673866589352499
Discovered a component with type 1
Vehicle is getting ready to arm
Arming...
Taking off...
[07:00:32|Debug] MAVLink: info: ARMED by Arm/Disarm component command (system_impl.cpp:257)
[07:00:32|Debug] MAVLink: info: [logger] /fs/microsd/log/2020-07-10/19_00_31.ulg (system_impl.cpp:257)
[07:00:32|Debug] MAVLink: info: Using minimum takeoff altitude: 2.50 m (system_impl.cpp:257)
[07:00:33|Debug] MAVLink: info: Takeoff detected (system_impl.cpp:257)
Landing...
Vehicle is landing...
[07:00:42|Debug] MAVLink: info: Landing at current position (system_impl.cpp:257)
Vehicle is landing...
Vehicle is landing...
Vehicle is landing...
Vehicle is landing...
Vehicle is landing...
Vehicle is landing...
[07:00:48|Debug] MAVLink: info: Landing detected (system_impl.cpp:257)
Landed!
[07:00:50|Debug] MAVLink: info: DISARMED by Auto disarm initiated (system_impl.cpp:257)
Finished...
```

## Optional: Build the Image, MAVSDK C++ and Example Program

Optionally, you can elect to build the docker image, MAVSDK C++ and the example program:

- connect VOXL Flight or VOXL + Flight Core hardware to host machine using USB
- run the build script:

```bash
./build-image.sh
```

Note: this process takes roughly 20 minutes, and concludes with a message like:

```bash
Step 15 : RUN make
 ---> Running in 49ac4a086286
Scanning dependencies of target takeoff_and_land
[ 50%] Building CXX object CMakeFiles/takeoff_and_land.dir/takeoff_and_land.cpp.o
[100%] Linking CXX executable takeoff_and_land
[100%] Built target takeoff_and_land
 ---> 863fffb74440
Removing intermediate container 49ac4a086286
Step 16 : CMD /bin/bash
 ---> Running in 654f51beed92
 ---> c005b845448f
Removing intermediate container 654f51beed92
Successfully built c005b845448f
```
