FROM arm64v8/ubuntu:bionic

WORKDIR /home

RUN apt-get update
RUN apt-get install cmake build-essential colordiff git doxygen -y
RUN apt install git -y

RUN git clone https://github.com/mavlink/MAVSDK.git

WORKDIR /home/MAVSDK
RUN git checkout main
RUN git submodule update --init --recursive

RUN cmake -DCMAKE_BUILD_TYPE=Debug -DBUILD_SHARED_LIBS=ON -Bbuild/default -H.
RUN cmake --build build/default --target install
RUN ldconfig

WORKDIR /home/MAVSDK/examples/takeoff_land
COPY takeoff_and_land.cpp .
RUN cmake .
RUN make

CMD ["/bin/bash"]
